
using System;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float distance;
    public float height ;
    public float damping ;
    public float rotationDamping ;

    void FixedUpdate () {
        Vector3 wantedPosition =target.TransformPoint(0, height, -distance);
        transform.position = Vector3.Lerp (transform.position, wantedPosition, Time.deltaTime * damping);
        Quaternion wantedRotation = Quaternion.LookRotation(target.position - transform.position, target.up);
        transform.rotation = Quaternion.Slerp (transform.rotation, wantedRotation, Time.deltaTime * rotationDamping);
    }
}
