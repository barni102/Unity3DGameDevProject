using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;



public class InputController: MonoBehaviour,  InputMaster.IPlayerActions
{
    public InputMaster controls;

    public Canvas canvas;
    

 

    public float ThrottleInput { get; private set; }

    public float SteerInput {get; private set; }
    // Start is called before the first frame update


    private void Awake()
    {
        
        controls = new InputMaster();
        controls.Player.SetCallbacks(this);
    }

    // Update is called once per frame
    
    private void OnDisable()
    {
        controls.Disable();
    }

    private void OnEnable()
    {
        controls.Enable();
    }
    
    public void OnPause(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            canvas.GetComponent<PauseMenu>().called();
        }

    }

    public void OnVerticalnput(InputAction.CallbackContext context)
    {
        ThrottleInput = context.ReadValue<float>();
    }

    public void OnHorizontalInput(InputAction.CallbackContext context)
    {
        SteerInput = context.ReadValue<float>();
    }
}
