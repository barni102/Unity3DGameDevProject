using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float BestLapTime { get; private set; } = Mathf.Infinity;
    public float LastLapTime { get; private set; } = 0;
    public float CurrentLapTime { get; private set; } = 0;
    public int CurrentLap { get; private set; } = 0;

    private float lapTimerTimestamp;
    private int lastCheckpointPassed = 0;

    private Transform checkpointsParent;

    private int checkpointCount;
    private int checkpointLayer;
    private Car carController;

    // Start is called before the first frame update
    private void Awake()
    {
        checkpointsParent = GameObject.Find("Checkpoints").transform;
        checkpointCount = checkpointsParent.childCount;
        checkpointLayer = LayerMask.NameToLayer("Checkpoint");
        carController = GetComponent<Car>();
    }

    void StartLap()
    {
        CurrentLap++;
        lastCheckpointPassed = 1;
        lapTimerTimestamp = Time.time;
    }

    void EndLap()
    {
        LastLapTime = Time.time - lapTimerTimestamp;
        BestLapTime = Math.Min(LastLapTime, BestLapTime);
    
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.layer != checkpointLayer)
        {
            return;
        }
        // ha ez az 1. ellenőrző pont...
        if (collider.gameObject.name=="1")
        {   
            //... és befejeztük a körünket
            if (lastCheckpointPassed == checkpointCount)
            {
                EndLap();
            }
            
            //ha ez az első lap vagy az utólsó ellenörzö pontnál vagyunk kezdjünk egy uj lapot
            if (CurrentLap == 0 || lastCheckpointPassed == checkpointCount)
            {
                StartLap();
            }
            return;
            
            
        }

        if (collider.gameObject.name == (lastCheckpointPassed + 1).ToString())
        {
            lastCheckpointPassed++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        CurrentLapTime = lapTimerTimestamp > 0 ? Time.time - lapTimerTimestamp : 0;
    
        
        carController.Steer = GameManager.Instance.InputController.SteerInput;
        carController.Throttle = GameManager.Instance.InputController.ThrottleInput;
    }
}
