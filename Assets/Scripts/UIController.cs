using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public Text UICurrentLapText;
    public Text UICurrentLapTimeText;
    public Text UILastLapTimeText;
    public Text UIBestLapTimeText;
    
    public Player UpdateUIForPlayer;

    private int currentLap=-1;
    private float currenLapTime;
    private float lastLapTime;
    private float bestLapTime;

  
    void Update()
    {
        if (UpdateUIForPlayer == null)
            return;

        if (UpdateUIForPlayer.CurrentLap != currentLap)
        {
            currentLap = UpdateUIForPlayer.CurrentLap;
            UICurrentLapText.text = $"LAP: {currentLap}";
        }

        if (UpdateUIForPlayer.CurrentLapTime != currenLapTime)
        {
            currenLapTime = UpdateUIForPlayer.CurrentLapTime;
            UICurrentLapTimeText.text = $"TIME: {(int) currenLapTime / 60}:{(currenLapTime)%60:00.000}";

        }
        if (UpdateUIForPlayer.LastLapTime != lastLapTime)
        {
            lastLapTime = UpdateUIForPlayer.LastLapTime;
            UILastLapTimeText.text = $"LAST: {(int) lastLapTime / 60}:{(lastLapTime)%60:00.000}";

        }
        if (UpdateUIForPlayer.BestLapTime != bestLapTime)
        {
            bestLapTime= UpdateUIForPlayer.BestLapTime;
            UIBestLapTimeText.text = bestLapTime < 1000000? $"BEST: {(int) bestLapTime / 60}:{(bestLapTime)%60:00.000}":"BEST: NONE";

        }
        
    }
}
