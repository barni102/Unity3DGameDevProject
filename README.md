# Unity3DGameDevProject

Ez a projekt a **Háromdimenziós játékok fejlesztése** kurzusra készül. Ebben a munkában meg szeretnék valósítani egy egyszerű autós játékot. A Játékba szeretnék implementálni több pályát és autót, amikkel körversenyezni lehet. Különös figyelmet szeretnék fordítani a fizikai megvalósitásra.

# Projekt Dokumentáció

## Irányitás

Az irányitás több eszközzel is lehetséges lenne (Xbox kontroller, billentyűzet). Szeretném kihasználni a kontorller előnyeit, és szabályozni a gyorsulás, lassulás és kanyarodás mértékét a ravasz/joystick pozíció alapján. A darabos mozgást szeretném kiküszöbölni az input simításával. Nehezítésként egy egszerűsitett váltó és kuplung mechanikát is hozzá szeretnék adni (opcionális).

## Fizika

Célom egy controller script elkészitése is, ami felel a testek fizikájáért. Szeretnék külön figyelni a sebességfokozatokra, fordulatszámra és különböző felületkre(pl. füvőn máshogy viselkedik az autó). Tervezek megvalositani egy olyan funkciót is, amivel előidézhető az autó guminak a kilyukadása.

## Játékmenet

Egy körversenyt szeretnék megvalositani, amiben az időmérés is lehetséges. Ezzel együtt pedig egy ranglistát is bevezetnék. MI-t nem szertnék bele implemntálni, ellenben többjátékos módot igen.

## Interfész

Lenne benne egy főképernyő, ahol el lehet inditani a játékot, igény szerint egyjátékos vagy többjátékos módban, illetve beállitások (hang, felbontás, váltó nehezsége) és kilépés. A játékban HUD is megjelenne amin sebességet, sebességfokozatot, fordulatszámot, időt és helyezést lehetne nyomon követni.

## Grafika

Low-poly, cartoon asseteket szeretnék használni. Lehetőség szerint nappal és éjszaka is lehetne versenyezni, ezen kivül kipufogó- és gumifűst is lenne.

## Nézet

Háromféle nézet kerülne megvalositásra: motorháztető nézet és hátsó (közeli, távoli), ezeket gombnyomásra lehetne váltani.

## Többjátékos mód

Két varáció elképzelhető: osztottképernyős megoldás, vagy online többjátékos mód. Ebből egyik valósulna meg.

## Tervezett források

A dizájnt a storeban található assetekkel szeretném megvalósitani. A játéklogikához nyílt forráskodú scripteket tervezek használni, amiket az igényeim szerint kibővitenék.

## Készitő

[Kovács Barnabás](https://gitlab.com/barni102/)
